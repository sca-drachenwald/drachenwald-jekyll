You'll need Docker running on your local machine.

Clone the repository that contains the Dockerfile and dependencies:

```
git clone git@gitlab.com:sca-drachenwald/drachenwald-jekyll.git
cd drachenwald-jekyll
```

The OS packages to be installed are in `Dockerfile`.  
The Ruby packages to be installed are in `Gemfile`.  
The Python packages to be installed are in `requirements.txt`.  

These files are no longer used in the main Drachenwald repository for builds in Gitlab CI/CD or from local builds using Docker. Gemfile is still needed for those who use a local Ruby installation without Docker.

If you don't already have a personal access token set up in Gitlab, you'll need to create one, using the [Gitlab instructions](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). Then log in to the container registry using:

```
docker login registry.gitlab.com -u <TOKEN NAME> -p <TOKEN>
```

To build the image for multiple architectures and push it to the registry:

```
docker build --push --platform linux/arm/v7,linux/arm64/v8,linux/amd64 -t registry.gitlab.com/sca-drachenwald/drachenwald-jekyll .
```

If, when you run this command, you get the message `error: multiple platforms feature is currently not supported for docker driver. Please switch to a different driver (eg. "docker buildx create --use")`, then run `docker buildx create --use` and try the above command again.

More info on the [Gitlab container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)

